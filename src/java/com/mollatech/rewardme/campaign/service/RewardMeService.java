/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.campaign.service;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmAiTrackingCampaignDetails;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmCampaigndetails;
import com.mollatech.rewardme.nucleus.db.RmCreditinfo;
import com.mollatech.rewardme.nucleus.db.RmSocialmediadetails;
import com.mollatech.rewardme.nucleus.db.RmSubscriptionpackagedetails;
import com.mollatech.rewardme.nucleus.db.connector.management.AITrackingManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.CampaignManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.CreditManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SocialMediaManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SubscriptionManagement;
import java.util.Date;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author abhishekingle
 */
@Path("RewardMeService")
public class RewardMeService {

     static final Logger logger = Logger.getLogger(RewardMeService.class);
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of RewardMeService
     */
    public RewardMeService() {
    }

   
   

    /**
     * PUT method for updating or creating an instance of RewardMeService
     * @param content representation for the resource
     */
    
     /**
     * Retrieves representation of an instance of com.mollatech.rewardme.nucleus.db.RmCampaigndetails
     * @return an instance of om.mollatech.rewardme.nucleus.db.RmCampaigndetails
     */
    @GET 
    @Path("/campaign/{campaignId}") 
    @Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON}) 
    public RmCampaigndetails getCampaign(@PathParam("campaignId") String campaignId){
        try {
                if(logger.getLevel() == null){
                logger.setLevel(Level.ALL);
                }
                logger.info("getCampaign API called at " + new Date());                
                logger.info("campaignId "+campaignId);
            RmCampaigndetails campaignDetails = new CampaignManagement().getCampaignByUniqueIdDetails(campaignId);          
            logger.info("getCampaign API Response "+campaignDetails);
            if (campaignDetails == null) {
            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
                .type(MediaType.APPLICATION_JSON).build());
            }
            
          return campaignDetails;
       }catch(Exception e){
           e.printStackTrace();
           throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON).build());
       }
    }
    
    @GET 
    @Path("/socialmedia/{socialMediaId}") 
    @Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON}) 
    public RmSocialmediadetails getSocialMediaSetting(@PathParam("socialMediaId") String socialMediaId){
        try {
            if(logger.getLevel() == null){
                logger.setLevel(Level.ALL);
            }
            logger.info("getSocialMediaSetting API called at " + new Date());                
            logger.info("socialMediaId "+socialMediaId);
            RmSocialmediadetails socailMediaDetails = new SocialMediaManagement().getSocialMediaDetailsByUniqueId(socialMediaId);          
            logger.info("getSocialMediaSetting API Response "+socailMediaDetails);
            if (socailMediaDetails == null) {
            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
                .type(MediaType.APPLICATION_JSON).build());
            }
          
          return socailMediaDetails;
       }catch(Exception e){
           e.printStackTrace();
           throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON).build());
       }
    }
    
    @GET 
    @Path("/brandowner/{brandownerId}") 
    @Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON}) 
    public RmBrandownerdetails getBrandOwnerDetails(@PathParam("brandownerId") int brandownerId){
        try {
          RmBrandownerdetails brandOwnerDetails = new BrandOwnerManagement().getBrandOwnerDetails(brandownerId);          
          if (brandOwnerDetails == null) {
            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
                .type(MediaType.APPLICATION_JSON).build());
            }
          return brandOwnerDetails;
       }catch(Exception e){
           e.printStackTrace();
           throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON).build());
       }
    }
    
@POST
@Path("/track/{campaignId}")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public ServiceStatus campaignTracking(@PathParam("campaignId") String campaignId, String resultantJsonData) {
        if(logger.getLevel() == null){
                logger.setLevel(Level.ALL);
            }
        logger.info("campaignTracking API called at " + new Date());                
        logger.info("campaignId "+campaignId);        
        if (campaignId == null || resultantJsonData == null) {
            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
                    .type(MediaType.APPLICATION_JSON).build());
        }
        ServiceStatus result = new ServiceStatus();
        RmCampaigndetails campaignDetails = new CampaignManagement().getCampaignByUniqueIdDetails(campaignId);
        try{
        if(campaignDetails != null){
            RmSubscriptionpackagedetails packageObj = new SubscriptionManagement().getSubscriptionbyOwnerId(campaignDetails.getOwnerId());
            if(packageObj != null){
                String creditDeduction = packageObj.getCreditDeductionConfiguration();
                if(creditDeduction != null){
                    JSONObject jsonObj = new JSONObject(creditDeduction);
                    String perUserCredit = null; String perCampaignCredit = null;
                    if(jsonObj.has("userSearched")){
                        perUserCredit = jsonObj.getString("userSearched");                
                    }                                        
                    if(perUserCredit == null && perCampaignCredit == null){                        
                        result.setResponseCode(-3);
                        result.setResponseMsg("Credit deduction setting not found");
                        return result;
                    }
                    Float userSearchedCredit = Float.parseFloat(perUserCredit);
                    RmCreditinfo creditDetails = new CreditManagement().getDetailsByOwnerId(campaignDetails.getOwnerId());
                    Float balanceCredit = creditDetails.getMainCredit();
                    Float estimatedTotalCredit = balanceCredit - userSearchedCredit;
                    if(estimatedTotalCredit > 0){
                        creditDetails.setMainCredit(estimatedTotalCredit);
                        new CreditManagement().updateDetails(creditDetails);                    
                        RmAiTrackingCampaignDetails aiTracking = new RmAiTrackingCampaignDetails();
                        aiTracking.setCampaignId(campaignDetails.getCampaignId());
                        aiTracking.setCreationDate(new Date());
                        aiTracking.setOwnerId(campaignDetails.getOwnerId());
                        aiTracking.setCreditDeducted(userSearchedCredit);
                        aiTracking.setTwitterReturnData(resultantJsonData);
                        aiTracking.setCampaignRunFlag(GlobalStatus.APPROVED);
                        aiTracking.setCampaignExecutedId(campaignDetails.getCurrentCampaignExecution());
                        new AITrackingManagement().addTracking(aiTracking);
                        result.setResponseCode(200);
                        result.setResponseMsg("Tracking added successfully");
                        logger.info("response code "+200);
                        logger.info("response message "+"Tracking added successfully");
                        return result;
                    }else{
                        result.setResponseCode(-4);
                        result.setResponseMsg("Not enough credit to deduct.");
                        logger.info("response code "+ -4);
                        logger.info("response message "+"Not enough credit to deduct.");
                        return result;
                    }
                }
            }else{
                result.setResponseCode(-2);
                result.setResponseMsg("Subscription details not found");
                logger.info("response code "+ -2);
                logger.info("response message "+"Subscription details not found");
                return result;
            }
            
        }else{
            result.setResponseCode(-1);
            result.setResponseMsg("Campaign details not found");
            logger.info("response code "+ -1);
            logger.info("response message "+"Campaign details not found");
            return result;
        }
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON).build());
        }
        return result;
    }
}
